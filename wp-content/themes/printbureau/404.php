<?php
/**
 * The template for displaying 404 pages.
 */

get_header(); ?>

<main role="main" class="wrap wrap-mobile wrap--padding">

	<div class="col">

		<div class="col-item col-item-full">

			<header class="content-header">
				<h1 class="content-header__title">Oops! That page can&rsquo;t be found.</h1>
			</header><!-- .content-header -->

		</div>

	</div><!-- .col -->

</main><!-- .main -->

<?php get_footer(); ?>