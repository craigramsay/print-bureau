jQuery(document).ready(function($){
    
    /*
     * Start Owl slider.
     */
    $("#owl-carousel-id").owlCarousel({
 
        // Most important owl features
        items : 1,
        itemsCustom : false,
        itemsDesktop : false,
        itemsDesktopSmall : false,
        itemsTablet: false,
        itemsTabletSmall: false,
        itemsMobile : false,
        singleItem : false,
        itemsScaleUp : false,
     
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 600,
        rewindSpeed : 1000,
     
        //Autoplay
        autoPlay : true,
        stopOnHover : true,
     
        // Navigation
        navigation : false,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,
     
        //Pagination
        pagination : true,
        paginationNumbers: false,
     
        // Responsive 
        responsive: true,
        responsiveRefreshRate : 0,
        responsiveBaseWidth: window,
     
        // CSS Styles
        baseClass : "owl-carousel",
        theme : "owl-theme",
     
        //Lazy load
        lazyLoad : false,
        lazyFollow : true,
        lazyEffect : "fade",
     
        //Auto height
        autoHeight : false,
     
        //JSON 
        jsonPath : false, 
        jsonSuccess : false,
     
        //Mouse Events
        dragBeforeAnimFinish : true,
        mouseDrag : true,
        touchDrag : true,
     
        //Transitions
        transitionStyle : false,
     
        // Other
        addClassActive : false,
     
        //Callbacks
        beforeUpdate : false,
        afterUpdate : false,
        beforeInit: false, 
        afterInit: false, 
        beforeMove: false, 
        afterMove: false,
        afterAction: false,
        startDragging : false,
        afterLazyLoad : false
 
    })

});