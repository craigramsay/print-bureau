<?php
/**
 * Functions
 */


/* ------------------------------------------------------------------------ *
 * Theme Setup
 * Init
 * Front End Scripts
 * Widgets
 * Login Screen
 * Setup Actions and Filters
 * Template Tags
 * MCE
 * Settings Plugin
 * Contact Form Plugin
 * ------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------ *
 * Theme Setup
 * ------------------------------------------------------------------------ */

if ( ! function_exists( 'malinky_setup' ) ) {

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function malinky_setup() {

		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Malinky Media, use a find and replace.
		 * to change 'malinky' to the name of your theme in all the template files.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/load_theme_textdomain
		 */
		load_theme_textdomain( 'malinky', get_template_directory() . '/languages' );


		/* -------------------------------- *
		 * Theme Support Section
		 * -------------------------------- */

		/**
		 * Enable support for Post Formats. A Child Theme overwrites these if redefined.
		 * Create different templates to display each of these content types.
		 *
		 * @link http://codex.wordpress.org/Post_Formats
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
		) );


		/**
		 * Enable support for Post Thumbnails. Is actually called Featured Image which can be attached to a post/page.
		 *
		 * @link http://codex.wordpress.org/Post_Thumbnails
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );


		/**
		 * Enable support for Custom Background. This is then added to Appearance menu.
 		 *
		 * @link http://codex.wordpress.org/Custom_Backgrounds
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Custom_Background
		 */
		//add_theme_support( 'custom-background' );


		/**
		 * Enable support for Custom Header. This is then added to Appearance menu.
 		 *
		 * @link http://codex.wordpress.org/Custom_Headers
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Custom_Header
		 */
		//add_theme_support( 'custom-header' );


		/**
		 * Add RSS feed links.
		 * These aren't added but if they are can also be removed from an action.
		 * remove_action('wp_head', 'feed_links', 2);
		 * remove_action('wp_head', 'feed_links_extra', 3);
 		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Feed_Links
		 */
		//add_theme_support( 'automatic-feed-links' );


		/**
		 * Enable support for HTML5 markup.
		 */
		add_theme_support( 'html5', array(
			'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'
		) );

		/**
		 * Enable support for Title Tag. Don't understand this fully so disabled.
		 */
		//add_theme_support( 'title-tag' );


		/* -------------------------------- *
		 * Image Sizes Section
		 * -------------------------------- */

		/**
		 * Featured image size.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
		 */
		//set_post_thumbnail_size( $width, $height, $crop );
	

		/**
		 * Additional image sizes.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_image_size
		 */
        add_image_size( 'malinky_carousel_large', 1600 );
        add_image_size( 'malinky_carousel_medium', 1200 );
        add_image_size( 'malinky_carousel_small', 600 );

		
		/* -------------------------------- *
		 * Other Section
		 * -------------------------------- */

		/**
		 * Register theme navigations. This uses a main navigation and footer in two locations.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/register_nav_menu
		 */
		register_nav_menus( array(
			'primary_navigation' 	=> __( 'Primary Navigation', 'malinky' ),
			'footer_navigation' => __( 'Footer Navigation', 'malinky' ),
		) );

		/**
		 * Include server side device detection.
		 *
		 * @link http://mobiledetect.net/
		 * @link https://github.com/serbanghita/Mobile-Detect/
		 */
		if ( ! is_admin() ) {

			if ( WP_ENV == 'local' ) {

			    require_once(ABSPATH . '/malinky-includes/Mobile_Detect.php');

			} elseif ( WP_ENV == 'dev' ) {

			    require_once(ABSPATH . '../../malinky-includes/Mobile_Detect.php');    

			} else {

			    require_once(ABSPATH . '../malinky-includes/Mobile_Detect.php');

			}

			global $malinky_mobile_detect;
			$malinky_mobile_detect = new Mobile_Detect();

			function malinky_is_phone()
			{
				global $malinky_mobile_detect;
				if ( $malinky_mobile_detect->isMobile() && ! $malinky_mobile_detect->isTablet() )
					return true;
			}

			function malinky_is_phone_tablet()
			{
				global $malinky_mobile_detect;
				if ( $malinky_mobile_detect->isMobile() || $malinky_mobile_detect->isTablet() )
					return true;
			}	

			function malinky_is_phone_computer()
			{
				global $malinky_mobile_detect;
				if ( ! $malinky_mobile_detect->isTablet() )
					return true;
			}						

			function malinky_is_tablet()
			{
				global $malinky_mobile_detect;
				if ( $malinky_mobile_detect->isTablet() )
					return true;
			}

			function malinky_is_tablet_computer()
			{
				global $malinky_mobile_detect;
				if ( $malinky_mobile_detect->isTablet() || ! $malinky_mobile_detect->isMobile() )
					return true;
			}			

			function malinky_is_computer()
			{
				global $malinky_mobile_detect;
				if ( ! $malinky_mobile_detect->isMobile() && ! $malinky_mobile_detect->isTablet() )
					return true;
			}	

		}

	}

}

add_action( 'after_setup_theme', 'malinky_setup' );





/* ------------------------------------------------------------------------ *
 * Init
 * ------------------------------------------------------------------------ */

/*function malinky_init() {}
add_action( 'init', 'malinky_init' );*/





/* ------------------------------------------------------------------------ *
 * Front End Scripts
 * ------------------------------------------------------------------------ */

/**
 * Enqueue frontend scripts.
 */
function malinky_scripts()
{

	/* -------------------------------- *
	 * Local && Dev && Prod
	 * -------------------------------- */

	/**
	 * Load WP jQuery and jQuery migrate in the footer.
	 */
	if ( ! is_admin() ) {

		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		wp_register_script( 'jquery',
							'/wp-includes/js/jquery/jquery.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'jquery-migrate',
							'/wp-includes/js/jquery/jquery-migrate.min.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery-migrate' );

	}


	if ( WP_ENV == 'local' ) {

		/* -------------------------------- *
		 * Local
		 * -------------------------------- */

		/**
		 * Stylesheet which includes normalize.
		 */
		wp_enqueue_style( 'malinky-style', get_stylesheet_uri() );

	
		/**
		 * Font awesome font.
		 *
		 * @link http://fortawesome.github.io/Font-Awesome/
		 */		
		wp_register_style( 'malinky-font-awesome', 
						   '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', 
						   false, 
						   NULL
		);
		wp_enqueue_style( 'malinky-font-awesome' );


		/**
		 * Modernizr which includes html5shiv.
		 *
		 * @link http://modernizr.com/
		 * @link https://github.com/aFarkas/html5shiv
		 */
		/*wp_register_script( 'malinky-modernizr-js',
							get_template_directory_uri() . '/js/modernizr-2.8.3.js',
							false,
							NULL
		);
		wp_enqueue_script( 'malinky-modernizr-js' );*/


		/*
		 * Malinky Media related javascript and jQuery.
		 */
		wp_register_script( 'malinky-main-js',
							get_template_directory_uri() . '/js/main.js',
							array( 'jquery' ),
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-main-js' );


		/**
		 * Owl Carousel Beta
		 *
		 * @link http://owlgraphic.com/
		 */
		wp_register_script( 'malinky-owl-carousel-js',
							get_template_directory_uri() . '/js/owl-carousel-beta.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-owl-carousel-js' );	


		/**
		 * Owl Carousel Setup.
		 *
		 * @link http://owlgraphic.com/
		 */
		wp_register_script( 'malinky-owl-carousel-setup-js',
							get_template_directory_uri() . '/js/owl-carousel-beta-setup.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-owl-carousel-setup-js' );


		/**
		 * Retina.
		 *
		 * @link http://imulus.github.io/retinajs/
		 */
		/*wp_register_script( 'malinky-retina-js',
							get_template_directory_uri() . '/js/retina.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-retina-js' );*/

	}


	if ( WP_ENV == 'dev' || WP_ENV == 'prod' ) {

		/* -------------------------------- *
		 * Dev && Prod
		 * -------------------------------- */

		/**
		 * Modernizr which includes html5shiv.
		 *
		 * @link http://modernizr.com/
		 * @link https://github.com/aFarkas/html5shiv
		 */
		/*wp_register_script( 'malinky-modernizr-min-js',
							get_template_directory_uri() . '/js/modernizr-2.8.3.min.js',
							false,
							NULL
		);
		wp_enqueue_script( 'malinky-modernizr-min-js' );*/


		/*
		 * googlemap.js, main.js, owl-carousel.js, owl-carousel-setup.js
		 */
		wp_register_script( 'malinky-scripts-min-js',
							get_template_directory_uri() . '/js/scripts.min.js',
							array( 'jquery' ),
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-scripts-min-js' );

	}


	/* -------------------------------- *
	 * Google Maps Local && Dev && Prod
	 * -------------------------------- */

	/**
	 * Google Map Set Up.
	 * If there are settings in admin.
	 * Localize them and then they are directly available as json in googlemap.js.
	 */
	$google_map_settings = get_option( '_000004_google_map_settings' );

		/*
		 * Check it exists (isn't false) then if on the correct page.
		 */
		if ( ! empty ( $google_map_settings['show_google_map'] ) && $google_map_settings['show_google_map'] ) {

			/* --------------- *
			 * Local
			 * No API Key
			 * --------------- */
			if ( WP_ENV == 'local' ) {

				/**
				 * Google Map Set Up.
				 * 
				 * Settings can be accessed from googlemap.js with google_map_settings.SETTING_KEY
				 *
				 * @link https://developers.google.com/maps/documentation/javascript/
				 */
				wp_register_script( 'malinky-googlemap-js',
									get_template_directory_uri() . '/js/googlemap.js',
									false,
									NULL,
									true
				);
				wp_localize_script( 'malinky-googlemap-js', 'google_map_settings', $google_map_settings );
				wp_enqueue_script( 'malinky-googlemap-js' );


				/**
				 * Load Google maps API without key.
				 * Uses malinky_initialize function which is in googlemap.js.
				 * Remember to set API Key.
				 *
				 * @link https://developers.google.com/maps/documentation/javascript/tutorial
				 */
				wp_register_script( 'malinky-googlemap-api-js', 
									'https://maps.googleapis.com/maps/api/js?callback=malinky_initialize', 
									false, 
									NULL, 
									true
				);

			}


			/* --------------- *
			 * Dev && Prod
			 * API Key
			 * --------------- */

			if ( WP_ENV == 'dev' || WP_ENV == 'prod' ) {

				/**
				 * Google Map Set Up.
				 * 
				 * Settings can be accessed from googlemap.js with google_map_settings.SETTING_KEY
				 *
				 * @link https://developers.google.com/maps/documentation/javascript/
				 */
				wp_register_script( 'malinky-googlemap-min-js',
									get_template_directory_uri() . '/js/googlemap.min.js',
									false,
									NULL,
									true
				);
				wp_localize_script( 'malinky-googlemap-min-js', 'google_map_settings', $google_map_settings );
				wp_enqueue_script( 'malinky-googlemap-min-js' );


				/*
				 * Get my default API Key if one isn't set.
				 */
				$google_map_settings['api_key'] == '' ? 'AIzaSyBC4B2o5cX8GuFyKrh1CpwtdVz7-j5ccOg' : $google_map_settings['api_key'];

				/**
				 * Load Google maps API without key.
				 * Uses malinky_initialize function which is in googlemap.js.
				 * Remember to set API Key.
				 *
				 * @link https://developers.google.com/maps/documentation/javascript/tutorial
				 */
				wp_register_script( 'malinky-googlemap-api-js', 
							'https://maps.googleapis.com/maps/api/js?key=' . $google_map_settings['api_key'] . '&callback=malinky_initialize', 
							false, 
							NULL, 
							true
				);

			}

			wp_enqueue_script( 'malinky-googlemap-api-js' );

		}

}

add_action( 'wp_enqueue_scripts', 'malinky_scripts' );


/**
 * Filter to amend the script tags that are loaded.
 * Currently adding async to the Google Map script tag.
 */
function malinky_javascript_loader( $tag, $handle, $src )
{
	
	if ( is_admin() ) return $tag;

	if ( $handle == 'malinky-googlemap-api-js' ) {
		$tag = str_replace('src=', 'async src=', $tag);
	}

	return $tag;

}

add_filter( 'script_loader_tag', 'malinky_javascript_loader', 10, 3 );





/* ------------------------------------------------------------------------ *
 * Widgets
 * ------------------------------------------------------------------------ */

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function malinky_widgets_init()
{

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'malinky' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar that appears on the left.', 'malinky' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

}

add_action( 'widgets_init', 'malinky_widgets_init' );





/* ------------------------------------------------------------------------ *
 * Login Screen
 * ------------------------------------------------------------------------ */

/**
 * Change login screen logo url.
 *
 * @return string
 */
function malinky_login_logo_url()
{
	return esc_url ( site_url( '', 'http' ) );
}

add_filter( 'login_headerurl', 'malinky_login_logo_url' );


/**
 * Remove shake on login.
 */
function malinky_login_shake()
{
    remove_action( 'login_head', 'wp_shake_js', 12 );
}

add_action( 'login_head', 'malinky_login_shake' );


/**
 * Style login logo and page.
 */
function malinky_login_screen()
{ ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ;?>/img/pb-logo-login.svg);
            background-size: 84px;
            width: auto;
        }
        body.login form {
			border: 1px solid #e5e5e5;
			box-shadow: none;
        }
        body.login form .input {
        	background: none;
        }
        body.login h1 a {
        	background-size: auto;
        }
        html.lt-ie9 body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ;?>'/img/pb-logo-login.png');
        }
        body.login .button.button-large {
        	height: auto;
        	line-height: normal;
		    padding: 10px;
        }        
        body.login .button-primary {
		    width: auto;
		    height: auto;
		    display: inline-block;
		    background: #000;
		    border: none;
		    border-radius: 0;
		    box-shadow: none;
        }
        body.login .button-primary:hover {
    		background: #000;
    		text-shadow: none;
    		box-shadow: none;
        }        
        body.login #nav {
        	text-shadow: none;
			padding: 26px 24px;
			background: #FFFFFF;
			border: 1px solid #e5e5e5;
			box-shadow: none;
			text-align: center;
		}
		body.login #nav a {
		    width: auto;
		    height: auto;
		    display: block;
		    padding: 10px;
		    background: #000;
		    border: none;
		    color: #fff;
		    border-radius: none;
		}
		body.login #nav a:hover {
			color: #fff;
			background: #000;
    		text-shadow: none;
    		box-shadow: none;
		}
		body.login #login #backtoblog a:hover {
			color: #999;
		}
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'malinky_login_screen' );





/* ------------------------------------------------------------------------ *
 * Setup Actions and Filters
 * ------------------------------------------------------------------------ */

remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link. xmlrpc.php
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);


/**
 * Add page slug to body class. Credit: Starkers Wordpress Theme.
 */
function malinky_add_slug_to_body_class($classes)
{

    global $post;

    if ( is_home() ) {

        $key = array_search( 'blog', $classes );

        if ( $key > -1 ) {

            unset( $classes[$key] );

        }

    } elseif ( is_page() ) {

        $classes[] = sanitize_html_class( $post->post_name );

    } elseif ( is_singular() ) {

        $classes[] = sanitize_html_class( $post->post_name );
    }

    return $classes;

}

add_filter( 'body_class', 'malinky_add_slug_to_body_class' ); // Add slug to body class (Starkers build)


/**
 * Remove wp_head() injected recent comment styles.
 */
function malinky_remove_recent_comments_style()
{

    global $wp_widget_factory;

    remove_action( 'wp_head',
    				array(
        				$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        				'recent_comments_style'
        			)
    );

}

add_action('widgets_init', 'malinky_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()


/**
 * CHECK
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {

	$content_width = 640; /* pixels */

}


/**
 * Hide admin bar for logged in users.
 */
show_admin_bar( false );


/**
 * Allow SVG into media uploader.
 */
function malinky_mime_types( $mimes )
{

	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
	
}

add_filter('upload_mimes', 'malinky_mime_types');


/**
 * Replace [...] from automatic excerpts.
 */
function malinky_excerpt( $more )
{

	return '...';

}

add_filter( 'excerpt_more', 'malinky_excerpt' );


if ( ! function_exists( 'malinky_read_more_text' ) )
{

	/**
	 * Return read more text for use in the_content and manually after the_excerpt().
	 */
	function malinky_read_more_text()
	{

		return '<span class="content-summary__more-link">Read More...</span>';

	}

}





/* ------------------------------------------------------------------------ *
 * Template Tags
 * ------------------------------------------------------------------------ */

/**
 * Get all page names (page_title).
 *
 * @param str $default Add a default option if page names are to be used in a dropdown.
 * @return array
 */
function malinky_get_page_names( $default )
{

	$malinky_all_pages = '';
	$malinky_page_titles = '';
	
	$args = array(
		'hierarchical' => 0
	);

	if ( $default )
		$malinky_page_titles[] = $default;

	$malinky_all_pages = get_pages( $args );
	foreach ( $malinky_all_pages as $key => $value ) {
		$malinky_page_titles[] = $malinky_all_pages[$key]->post_title;
	}

	return $malinky_page_titles;

}


/**
 * Climb to parent page and return slug.
 *
 * @return string
 */
function malinky_tree()
{
	$parent_post_name = '';
  	if( is_page() ) {
  		global $post;
      	/* Get an array of Ancestors and Parents if they exist */
  		$parents = get_post_ancestors( $post->ID );
      	/* Get the top Level page->ID count base 1, array base 0 so -1 */ 
  		$id = ( $parents ) ? $parents[count( $parents )-1]: $post->ID;
     	/* Get the parent and set the $parent_post_name with the page slug (post_name) */
  		$parent = get_page( $id );
  		$parent_post_name = $parent->post_name;
	}
	return $parent_post_name;
}





if ( ! function_exists( 'malinky_image_url' ) ) {

	/**
	 * Output an image URL based on the attachment id and size.
	 *
	 * @param string $attachment_id The attachment id
	 * @param string $attachment_size The attachment image_size to output
	 * @return str
	 */
	function malinky_image_url( $attachment_id, $attachment_size )
	{
		$malinky_attachment = wp_get_attachment_image_src( $attachment_id, $attachment_size );
		if ($malinky_attachment) return $malinky_attachment[0];
	}

}


if ( ! function_exists( 'malinky_truncate_words' ) ) {

	/**
	 * Truncate a string.
	 *
	 * @param string $text
	 * @param int $length
	 * @return string
	 */
	function malinky_truncate_words( $text, $length )
	{
	   $length = abs( ( int) $length );
	   if( strlen( $text ) > $length ) {
	      $text = preg_replace( "/^(.{1,$length})(\s.*|$)/s", '\\1...', $text );
	   }
	   return $text;
	}

}


if ( ! function_exists( 'malinky_content_meta' ) ) {
	/**
	 * Posted and updated dates and author name / link.
	 *
	 * @param bool $show_author Set to false to hide author details.
	 */
	function malinky_content_meta( $show_author = true )
	{

		$posted_time = '';
		$updated_string = '';
		$author = '';

		$posted_time = sprintf( 
			'<time class="content-header__meta__date--published" datetime="%1$s">%2$s</time>', 
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);

		$posted_string = 'Posted on ' . $posted_time;

		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

			$updated_time = sprintf( 
				'<time class="content-header__meta__date--updated" datetime="%1$s">%2$s</time>', 
				esc_attr( get_the_modified_date( 'c' ) ),
				esc_html( get_the_modified_date() )
			);

			$updated_string = ', Updated on ' . $updated_time;

		}

		$author = '<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>';

		if ( ! $show_author )
			return '<span class="content-header__meta__date">' . $posted_string . $updated_string . '</span>';

		return '<span class="content-header__meta__date">' . $posted_string . $updated_string . '</span><span class="byline"> by ' . $author . '</span>';

	}

}


if ( ! function_exists( 'malinky_content_footer' ) ) {

	/**
	 * Post categories, tags and edit link.
	 */
	function malinky_content_footer()
	{

		$categories = '';
		$tags = '';

		//Only show for posts.
		if ( get_post_type() == 'post' ) {

			$categories_list = get_the_category_list( ', ' );
			if ( $categories_list ) {
				$categories = sprintf( '<span class="content-footer__cat-link">Posted in %1$s</span>', $categories_list );
			}

			$tags_list = get_the_tag_list( '', ', ' );
			if ( $tags_list ) {
				$tags = sprintf( '<span class="content-footer__tag-link">Tagged with %1$s</span>', $tags_list );
			}

		}

		$edit_link = sprintf('<span class="content-footer__edit-link"><a href="%1$s">Edit</a></span>', esc_url( get_edit_post_link() ) );

		return $categories . ' ' . $tags . ' ' . $edit_link;

	}

}


if ( ! function_exists( 'malinky_posts_pagination' ) ) {

	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function malinky_posts_pagination()
	{

		//Return if only 1 page
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		} ?>

		<nav class="col posts-pagination" role="navigation">

			<div class="col-item col-item-half posts-pagination__link posts-pagination__link--older">
				<?php if ( get_next_posts_link() ) { ?>
						<?php next_posts_link( 'Older Posts' ); ?>
				<?php } ?>
			</div><!--

			--><div class="col-item col-item-half col-item--align-right posts-pagination__link posts-pagination__link--newer">
				<?php if ( get_previous_posts_link() ) { ?>
						<?php previous_posts_link( 'Newer Posts' ); ?>
				<?php } ?>			
			</div>

		</nav><!-- .posts-pagination -->

	<?php }

}


if ( ! function_exists( 'malinky_post_pagination' ) ) {

	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function malinky_post_pagination()
	{

		//Return if no navigation.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) return; ?>

		<nav class="col post-pagination" role="navigation">

			<div class="col-item col-item-half post-pagination__link post-pagination__link--older">
				<?php previous_post_link( '%link', '%title' ); ?>
			</div><!--

			--><div class="col-item col-item-half col-item--align-right post-pagination__link post-pagination__link--newer">
				<?php next_post_link( '%link', '%title' ); ?>
			</div>

		</nav><!-- .post-pagination -->
		
	<?php }

}


if ( ! function_exists( 'malinky_archive_title' ) ) {

	/**
	 * Shim for `the_archive_title()`.
	 *
	 * Display the archive title based on the queried object.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 *
	 * @param string $before Optional. Content to prepend to the title. Default empty.
	 * @param string $after  Optional. Content to append to the title. Default empty.
	 */
	function malinky_archive_title( $before = '', $after = '' )
	{

		if ( is_category() ) {
			$title = sprintf( __( 'Category: %s', 'malinky' ), single_cat_title( '', false ) );
		} elseif ( is_tag() ) {
			$title = sprintf( __( 'Tag: %s', 'malinky' ), single_tag_title( '', false ) );
		} elseif ( is_author() ) {
			$title = sprintf( __( 'Author: %s', 'malinky' ), '<span class="vcard">' . get_the_author() . '</span>' );
		} elseif ( is_year() ) {
			$title = sprintf( __( 'Year: %s', 'malinky' ), get_the_date( _x( 'Y', 'yearly archives date format', 'malinky' ) ) );
		} elseif ( is_month() ) {
			$title = sprintf( __( 'Month: %s', 'malinky' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'malinky' ) ) );
		} elseif ( is_day() ) {
			$title = sprintf( __( 'Day: %s', 'malinky' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'malinky' ) ) );
		} elseif ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'malinky' );
		} elseif ( is_post_type_archive() ) {
			$title = sprintf( __( 'Archives: %s', 'malinky' ), post_type_archive_title( '', false ) );
		} elseif ( is_tax() ) {
			$tax = get_taxonomy( get_queried_object()->taxonomy );
			/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( __( '%1$s: %2$s', 'malinky' ), $tax->labels->singular_name, single_term_title( '', false ) );
		} else {
			$title = __( 'Archives', 'malinky' );
		}

		/**
		 * Filter the archive title.
		 *
		 * @param string $title Archive title to be displayed.
		 */
		$title = apply_filters( 'get_the_archive_title', $title );

		if ( ! empty( $title ) ) {
			echo $before . $title . $after;
		}

	}

}


if ( ! function_exists( 'malinky_archive_description' ) ) {

	/**
	 * Shim for `the_archive_description()`.
	 *
	 * Display category, tag, or term description.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 *
	 * @param string $before Optional. Content to prepend to the description. Default empty.
	 * @param string $after  Optional. Content to append to the description. Default empty.
	 */
	function malinky_archive_description( $before = '', $after = '' )
	{

		$description = apply_filters( 'get_the_archive_description', term_description() );

		if ( ! empty( $description ) ) {
			/**
			 * Filter the archive description.
			 *
			 * @see term_description()
			 *
			 * @param string $description Archive description to be displayed.
			 */
			echo $before . $description . $after;
		}

	}

}


/**
 * Display SVG logo.
 */
function display_svg_logo()
{
?>
	<svg class="svg-logo" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="74px" height="70px" viewBox="0 156.296 150 142.793" xml:space="preserve">
		<g>
			<path fill="#fff" d="M32.917,298.966v-51.301h30.06c15.499,0,27.423-3.481,35.775-10.452
			c8.353-6.969,12.529-18.875,12.529-35.717c0-15.422-4.198-26.81-12.592-34.166c-8.398-7.358-19.656-11.034-33.776-11.034
			l37.012,0.114c17.394,0.26,29.72,5.324,36.975,15.199c4.366,6.062,6.55,13.321,6.55,21.778c0,8.709-2.202,15.711-6.606,21.001
			c-2.466,2.972-6.096,5.677-10.884,8.132c7.281,2.647,12.775,6.841,16.48,12.581c3.706,5.743,5.561,12.712,5.561,20.909
			c0,8.452-2.121,16.035-6.353,22.747c-2.696,4.45-6.064,8.196-10.105,11.227c-4.556,3.482-9.929,5.87-16.123,7.163
			c-6.188,1.289-12.908,1.936-20.158,1.936l-64.344,0.007" />
			<path fill="#fff" d="M75.605,185.921c-3.769-3.155-9.054-4.737-15.848-4.737h-26.84v41.962h26.839
			c6.794,0,12.079-1.708,15.848-5.12c3.773-3.417,5.657-8.835,5.657-16.249C81.261,194.371,79.378,189.081,75.605,185.921" />
		</g>
	</svg>
<?php
}


/**
 * Display SVG logo for fixed main_navigation (not mobile).
 */
function display_svg_logo_fixed()
{
?>
	<svg class="svg-logo-fixed" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="36px" height="34px" viewBox="0 156.296 150 142.793" xml:space="preserve">
		<g>
			<path fill="#fff" d="M32.917,298.966v-51.301h30.06c15.499,0,27.423-3.481,35.775-10.452
			c8.353-6.969,12.529-18.875,12.529-35.717c0-15.422-4.198-26.81-12.592-34.166c-8.398-7.358-19.656-11.034-33.776-11.034
			l37.012,0.114c17.394,0.26,29.72,5.324,36.975,15.199c4.366,6.062,6.55,13.321,6.55,21.778c0,8.709-2.202,15.711-6.606,21.001
			c-2.466,2.972-6.096,5.677-10.884,8.132c7.281,2.647,12.775,6.841,16.48,12.581c3.706,5.743,5.561,12.712,5.561,20.909
			c0,8.452-2.121,16.035-6.353,22.747c-2.696,4.45-6.064,8.196-10.105,11.227c-4.556,3.482-9.929,5.87-16.123,7.163
			c-6.188,1.289-12.908,1.936-20.158,1.936l-64.344,0.007" />
			<path fill="#fff" d="M75.605,185.921c-3.769-3.155-9.054-4.737-15.848-4.737h-26.84v41.962h26.839
			c6.794,0,12.079-1.708,15.848-5.12c3.773-3.417,5.657-8.835,5.657-16.249C81.261,194.371,79.378,189.081,75.605,185.921" />
		</g>
	</svg>
<?php
}


/**
 * Display SVG logo text.
 */
function display_svg_logo_text()
{
?>

	<svg class="svg-logo-text" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="103px" height="70px" viewBox="0 137.195 150 102.227" xml:space="preserve">
		<g>
			<path fill="#fff" d="M74.79,211.799h4.875l9.364,21.654h0.079l9.441-21.654h4.759v27.622h-3.274v-23.174h-0.082l-9.908,23.174
			h-1.988l-9.912-23.174h-0.078v23.174H74.79V211.799L74.79,211.799z M59.339,216.011h-0.078l-5.733,13.577h11.274L59.339,216.011z
			 M57.856,211.799h3.083l11.742,27.622H68.86l-2.81-6.788H52.239l-2.848,6.788h-3.706L57.856,211.799z M25.397,211.799h17.36v3.042
			H28.671v8.664h13.15v3.041h-13.15v9.833h14.788v3.042H25.397V211.799z M21.535,214.841h-9.131v24.58H9.125v-24.58H0v-3.042h21.536
			V214.841z M149.999,191.403c0,1.604-0.244,3.085-0.727,4.435c-0.486,1.349-1.203,2.521-2.158,3.517
			c-0.955,0.993-2.149,1.771-3.575,2.331c-1.426,0.559-3.069,0.842-4.929,0.842c-1.885,0-3.543-0.283-4.97-0.842
			c-1.428-0.56-2.618-1.338-3.575-2.331c-0.953-0.995-1.674-2.168-2.155-3.517c-0.488-1.35-0.73-2.83-0.73-4.435v-16.624h5.964
			v16.396c0,0.841,0.134,1.615,0.397,2.331c0.271,0.713,0.647,1.333,1.131,1.853c0.481,0.524,1.06,0.932,1.74,1.224
			c0.674,0.295,1.407,0.44,2.197,0.44c0.787,0,1.515-0.146,2.178-0.44c0.662-0.292,1.236-0.699,1.721-1.224
			c0.484-0.52,0.86-1.14,1.125-1.853c0.269-0.716,0.401-1.49,0.401-2.331v-16.396H150L149.999,191.403L149.999,191.403z
			 M112.735,182.649l-3.288,8.411h6.612L112.735,182.649z M110.443,174.779h4.929l11.772,27.06h-6.725l-2.335-5.733h-10.505
			l-2.259,5.733h-6.572L110.443,174.779z M77.687,174.779h18.387v5.502H83.649v5.045h11.735v5.503H83.649v5.504h13.113v5.505H77.687
			V174.779L77.687,174.779z M58.655,185.978h3.517c0.533,0,1.101-0.021,1.701-0.058c0.597-0.039,1.141-0.155,1.624-0.345
			c0.484-0.189,0.888-0.49,1.205-0.897c0.318-0.409,0.479-0.98,0.479-1.723c0-0.686-0.141-1.234-0.422-1.643
			c-0.281-0.407-0.636-0.717-1.069-0.934c-0.436-0.219-0.932-0.362-1.49-0.44c-0.562-0.077-1.108-0.114-1.646-0.114h-3.898V185.978
			L58.655,185.978z M52.693,174.779h10.47c1.378,0,2.684,0.134,3.92,0.399c1.234,0.267,2.319,0.717,3.247,1.337
			c0.932,0.627,1.668,1.468,2.219,2.526c0.546,1.056,0.821,2.375,0.821,3.955c0,1.911-0.496,3.534-1.492,4.873
			c-0.992,1.336-2.446,2.183-4.356,2.542l6.878,11.428h-7.146l-5.658-10.815h-2.943v10.815h-5.961V174.779L52.693,174.779z
			 M48.795,191.403c0,1.604-0.241,3.085-0.727,4.435c-0.482,1.349-1.205,2.521-2.156,3.517c-0.958,0.993-2.147,1.771-3.577,2.331
			c-1.426,0.559-3.069,0.842-4.929,0.842c-1.885,0-3.542-0.283-4.968-0.842c-1.43-0.56-2.62-1.338-3.573-2.331
			c-0.959-0.995-1.68-2.168-2.158-3.517c-0.488-1.35-0.729-2.83-0.729-4.435v-16.624h5.96v16.396c0,0.841,0.136,1.615,0.407,2.331
			c0.264,0.713,0.64,1.333,1.124,1.853c0.48,0.524,1.064,0.932,1.736,1.224c0.678,0.295,1.411,0.44,2.201,0.44
			c0.787,0,1.513-0.146,2.178-0.44c0.663-0.292,1.234-0.699,1.72-1.224c0.484-0.52,0.86-1.14,1.128-1.853
			c0.267-0.716,0.403-1.49,0.403-2.331v-16.396h5.96L48.795,191.403L48.795,191.403z M8.281,196.794h5.352
			c0.458,0,0.926-0.054,1.397-0.154c0.471-0.103,0.897-0.283,1.28-0.535c0.38-0.254,0.694-0.589,0.934-0.995
			c0.244-0.405,0.364-0.903,0.364-1.49c0-0.638-0.159-1.153-0.477-1.549c-0.321-0.393-0.721-0.693-1.205-0.896
			c-0.484-0.206-1.007-0.345-1.567-0.423c-0.562-0.073-1.083-0.112-1.566-0.112H8.281V196.794z M8.281,185.594h4.321
			c0.458,0,0.907-0.052,1.356-0.153c0.446-0.101,0.849-0.268,1.202-0.496c0.358-0.231,0.647-0.535,0.862-0.918
			s0.326-0.841,0.326-1.377c0-0.56-0.123-1.023-0.362-1.393c-0.244-0.37-0.555-0.655-0.938-0.86
			c-0.384-0.204-0.814-0.349-1.298-0.438c-0.484-0.091-0.956-0.133-1.415-0.133H8.281V185.594z M2.321,174.779h10.091
			c1.17,0,2.348,0.083,3.534,0.248s2.248,0.496,3.191,0.992c0.943,0.496,1.707,1.194,2.292,2.083
			c0.585,0.894,0.879,2.079,0.879,3.556c0,1.529-0.426,2.796-1.279,3.801c-0.856,1.008-1.98,1.729-3.383,2.163v0.073
			c0.891,0.13,1.701,0.378,2.426,0.744c0.727,0.372,1.352,0.842,1.872,1.418c0.523,0.573,0.922,1.242,1.205,2.005
			c0.279,0.764,0.422,1.579,0.422,2.445c0,1.429-0.306,2.619-0.914,3.575c-0.617,0.953-1.407,1.728-2.375,2.312
			c-0.967,0.587-2.056,1.008-3.265,1.262c-1.211,0.253-2.417,0.383-3.614,0.383H2.321V174.779z M96.911,142.465h-7.719v-5.27h21.403
			v5.27h-7.721v21.788h-5.964V142.465z M60.528,137.195h8.103l11.501,18.802h0.077v-18.802h5.964v27.058h-7.797l-11.811-19.263
			h-0.077v19.263h-5.96V137.195L60.528,137.195z M49.752,137.195h5.964v27.058h-5.964V137.195L49.752,137.195z M31.023,148.392h3.515
			c0.535,0,1.101-0.019,1.702-0.058c0.599-0.039,1.139-0.151,1.627-0.345c0.481-0.19,0.882-0.491,1.202-0.899
			c0.32-0.407,0.477-0.978,0.477-1.716c0-0.69-0.14-1.234-0.418-1.645c-0.283-0.407-0.638-0.719-1.069-0.936
			c-0.434-0.217-0.93-0.362-1.492-0.439c-0.562-0.078-1.112-0.115-1.643-0.115h-3.898L31.023,148.392L31.023,148.392z M25.06,137.195
			h10.472c1.376,0,2.68,0.131,3.92,0.397c1.232,0.269,2.317,0.717,3.247,1.339c0.93,0.626,1.668,1.467,2.217,2.525
			c0.55,1.058,0.821,2.375,0.821,3.955c0,1.91-0.496,3.536-1.492,4.873c-0.992,1.339-2.445,2.184-4.355,2.544l6.878,11.426h-7.145
			l-5.658-10.815h-2.941v10.815H25.06V137.195z M8.281,148.657h3.976c0.537,0,1.05-0.049,1.55-0.152
			c0.496-0.103,0.942-0.273,1.333-0.517c0.399-0.241,0.715-0.572,0.959-0.992c0.238-0.42,0.362-0.949,0.362-1.587
			c0-0.688-0.159-1.244-0.477-1.663c-0.32-0.42-0.727-0.746-1.225-0.977c-0.496-0.229-1.05-0.372-1.66-0.438
			c-0.615-0.064-1.202-0.095-1.762-0.095H8.281V148.657z M2.321,137.195h10.054c1.399,0,2.722,0.124,3.972,0.379
			c1.25,0.255,2.338,0.696,3.265,1.318c0.934,0.626,1.672,1.466,2.22,2.524c0.546,1.056,0.826,2.387,0.826,3.994
			c0,1.579-0.26,2.904-0.768,3.974c-0.511,1.071-1.212,1.924-2.104,2.561c-0.891,0.638-1.949,1.089-3.17,1.356
			c-1.225,0.269-2.55,0.401-3.978,0.401H8.281v10.55h-5.96V137.195z" />
		</g>
	</svg>

<?php
}





/* ------------------------------------------------------------------------ *
 * Tiny MCE
 * ------------------------------------------------------------------------ */

/**
 * Callback function to insert 'styleselect' into the $buttons array.
 */
function malinky_mce_insert_formats_dropdown( $buttons )
{

	array_unshift( $buttons, 'styleselect' );
	return $buttons;

}

add_filter('mce_buttons_2', 'malinky_mce_insert_formats_dropdown');


/**
 * Callback function to filter the MCE settings and insert formats into the new dropdown.
 */
function malinky_mce_insert_formats( $init_array )
{  

	$style_formats = array(  
		array(  
			'title' => 'Heading 1 Style',
    		'block' => 'p',
    		'classes' => 'heading-1'
		),
		array(  
			'title' => 'Heading 2 Style',
    		'block' => 'p',
    		'classes' => 'heading-2'
		),
		array(  
			'title' => 'Heading 3 Style',
    		'block' => 'p',
    		'classes' => 'heading-3'
		),
		array(  
			'title' => 'Heading 4 Style',
    		'block' => 'p',
    		'classes' => 'heading-4'
		),
		array(  
			'title' => 'Heading 5 Style',
    		'block' => 'p',
    		'classes' => 'heading-5'
		),						
		array(  
			'title' => 'Heading 6 Style',
    		'block' => 'p',
    		'classes' => 'heading-6'
		),
		array(
			'title' => 'Text No Margin',
    		'selector' => 'p, h1, h2, h3, h4, h5, h6',
    		'classes' => 'no-margin'
		),
		array(  
			'title' => 'White Text',
    		'selector' => 'p, h1, h2, h3, h4, h5, h6',
    		'classes' => 'text-white'
		),
		array(  
			'title' => 'Italic Text',
    		'selector' => 'p, h1, h2, h3, h4, h5, h6',
    		'classes' => 'italic-text'
		)		
	);

	/*
	 * Insert the array, JSON ENCODED, into 'style_formats'.
	 */
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;
  
} 

add_filter( 'tiny_mce_before_init', 'malinky_mce_insert_formats' );


/**
 * Add the above formats to the quicktag buttons in the Text/HTML editor.
 */ 
function malinky_mce_quicktags()
{

    if ( wp_script_is( 'quicktags' ) ) { ?>

	    <script type="text/javascript">
		    QTags.addButton( 'mm_heading1', 'mm_heading1', '<p class="heading-1">', '</p>', '', 'Heading 1 Style' );
		    QTags.addButton( 'mm_heading2', 'mm_heading2', '<p class="heading-2">', '</p>', '', 'Heading 2 Style' );
		    QTags.addButton( 'mm_heading3', 'mm_heading3', '<p class="heading-3">', '</p>', '', 'Heading 3 Style' );
		    QTags.addButton( 'mm_heading4', 'mm_heading4', '<p class="heading-4">', '</p>', '', 'Heading 4 Style' );
		    QTags.addButton( 'mm_heading5', 'mm_heading5', '<p class="heading-5">', '</p>', '', 'Heading 5 Style' );
		    QTags.addButton( 'mm_heading6', 'mm_heading6', '<p class="heading-6">', '</p>', '', 'Heading 6 Style' );
	    </script>

	<?php
    }

}

add_action( 'admin_print_footer_scripts', 'malinky_mce_quicktags' );





/* ------------------------------------------------------------------------ *
 * Settings Plugin
 * ------------------------------------------------------------------------ */

if ( class_exists( 'Malinky_Settings_Plugin' ) ) {

	global $settings_object;

	$master_args = array(
		0 => array(
			'malinky_settings_page_title' 			=> 'Site Settings',
			'malinky_settings_menu_title' 			=> 'Site Settings',
			'malinky_settings_page_parent_slug'		=> 'options-general.php',
			'malinky_settings_page_tabs'			=> array(),
			'malinky_settings_capability' 			=> 'manage_options',
			'malinky_settings_sections' 			=> array(
				array(
					'section_title' 	=> 'Contact Information',
					'section_intro' 	=> '',
				),
				array(
					'section_title' 	=> 'Contact Form Settings',
					'section_intro' 	=> '',
				),
				array(
					'section_title' 	=> 'Google Analytics Setting',
					'section_intro' 	=> '',
				),
				array(
					'section_title' 	=> 'Google Map Settings',
					'section_intro' 	=> '',
				)				
			),
			'malinky_settings_fields' 			=> array(
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Address',
					'option_field_type' 		=> 'editor_field',
					'option_field_type_options' => array(
					),
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),			
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Email Address',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'email'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Phone Number',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'phone'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Mobile Number',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'phone'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Facebook Account',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'url'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Twitter Account',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'url'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Google Plus Account',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'url'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'LinkedIn Account',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'url'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Opening Days',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Information',
					'option_title' 				=> 'Opening Hours',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Information',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),																							
				array(
					'option_group_name' 		=> 'Contact Form Settings',
					'option_title' 				=> 'Contact Form Page',
					'option_field_type' 		=> 'select_field',
					'option_field_type_options' => malinky_get_page_names( 'Disabled' ),
					'option_section' 			=> 'Contact Form Settings',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Form Settings',
					'option_title' 				=> 'Contacted Message',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),
					'option_section' 			=> 'Contact Form Settings',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> 'If left blank will display: Thanks for contacting us, we will be in touch as soon as possible.',
					'option_default'			=> array(
						''
					)
				),								
				array(
					'option_group_name' 		=> 'Contact Form Settings',
					'option_title' 				=> 'Email Address',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Form Settings',
					'option_validation' 		=> array(
						'email'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Form Settings',
					'option_title' 				=> 'Email Password',
					'option_field_type' 		=> 'password_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Form Settings',
					'option_validation' 		=> array(),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Form Settings',
					'option_title' 				=> 'Email Host',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Form Settings',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Contact Form Settings',
					'option_title' 				=> 'Email Port',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Contact Form Settings',
					'option_validation' 		=> array(
						'numbers'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> '',
					'option_title' 				=> 'Google Analytics',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Analytics Setting',
					'option_validation' 		=> array(
						'googleua'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Google Map Settings',
					'option_title' 				=> 'Show Google Map',
					'option_field_type' 		=> 'checkbox_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Map Settings',
					'option_validation' 		=> array(),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Google Map Settings',
					'option_title' 				=> 'API Key',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Map Settings',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> 'Leave blank to use Malinky Media\'s API Key.',
					'option_default'			=> array(
						''
					)
				),				
				array(
					'option_group_name' 		=> 'Google Map Settings',
					'option_title' 				=> 'Lat',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Map Settings',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Google Map Settings',
					'option_title' 				=> 'Long',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Map Settings',
					'option_validation' 		=> array(
						'text'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				),
				array(
					'option_group_name' 		=> 'Google Map Settings',
					'option_title' 				=> 'Zoom',
					'option_field_type' 		=> 'text_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Map Settings',
					'option_validation' 		=> array(
						'numbers'
					),
					'option_placeholder'		=> '',
					'option_description'		=> '14 is the recommended default.',
					'option_default'			=> array(
						'14'
					)
				),
				array(
					'option_group_name' 		=> 'Google Map Settings',
					'option_title' 				=> 'Show Address',
					'option_field_type' 		=> 'checkbox_field',
					'option_field_type_options' => array(
					),			
					'option_section' 			=> 'Google Map Settings',
					'option_validation' 		=> array(),
					'option_placeholder'		=> '',
					'option_description'		=> '',
					'option_default'			=> array(
						''
					)
				)
			)
		)
	);

	foreach ($master_args as $k => $v) {
		$settings_object = 'malinky_settings_plugin_' . $k;
		$settings_object = new Malinky_Settings_Plugin($master_args[$k]);
	}

	//echo $settings_object->malinky_settings_get_option_functions($settings_object->all_option_names);
	//print_r($settings_object->all_option_names);

}


/**
 * Shortcode to display Google Map.
 */
function malinky_google_map_shortcode()
{

	ob_start(); ?>

		<section class="wrap-full">
			<div class="col">
				<div class="col_item col_item_full">
					<div id="map-canvas"></div>
				</div>
			</div><!-- .col -->
		</section><!-- .wrap-full -->

	<?php return ob_get_clean();

}

add_shortcode( 'malinky-google-map', 'malinky_google_map_shortcode' );





/* ------------------------------------------------------------------------ *
 * Contact Form Plugin
 * ------------------------------------------------------------------------ */

/**
 * Shortcode to display the contact form.
 */
function malinky_contact_form_shortcode()
{

	global $malinky_error_messages;

	$name 		= isset( $_POST['malinky_name'] ) ? $_POST['malinky_name'] : '';
	$company	= isset( $_POST['malinky_company'] ) ? $_POST['malinky_company'] : '';
	$email 		= isset( $_POST['malinky_email'] ) ? $_POST['malinky_email'] : '';
	$phone 		= isset( $_POST['malinky_phone'] ) ? $_POST['malinky_phone'] : '';	
	$message 	= isset( $_POST['malinky_message'] ) ? $_POST['malinky_message'] : '';
	?>

	<?php ob_start(); ?>

	<section class="wrap-full" id="contact-success">
		<div class="wrap">

			<div class="col col--align-center contact-form">
				<div class="col-item col-item-half col-item-three-quarter--medium col-item-full--small">

					<form action="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>" method="post" role="form">

						<label for="malinky_name">Name*</label>
						<input type="text" name="malinky_name" id="malinky_name" placeholder="" value="<?php echo esc_attr( wp_unslash( $name ) ); ?>" required>
						<?php if (isset($malinky_error_messages['name_error'][0])) echo '<p class="box error">' . $malinky_error_messages['name_error'][0] . '</p>'; ?>

						<label for="malinky_company">Company</label>
						<input type="text" name="malinky_company" id="malinky_company" placeholder="" value="<?php echo esc_attr( wp_unslash( $company ) ); ?>">

						<label for="malinky_email">Email*</label>
						<input type="email" name="malinky_email" id="malinky_email" placeholder="" value="<?php echo esc_attr( wp_unslash( $email ) ); ?>" required>
						<?php if (isset($malinky_error_messages['email_error'][0])) echo '<p class="box error">' . $malinky_error_messages['email_error'][0] . '</p>'; ?>
						
						<label for="malinky_phone">Phone</label>
						<input type="tel" name="malinky_phone" id="malinky_phone" value="<?php echo esc_attr( wp_unslash( $phone ) ); ?>">
						<?php if (isset($malinky_error_messages['phone_error'][0])) echo '<p class="box error">' . $malinky_error_messages['phone_error'][0] . '</p>'; ?>

						<label for="malinky_message">Message*</label>
						<textarea name="malinky_message" id="malinky_message" rows="10" cols="30" required><?php echo esc_textarea( wp_unslash( $message ) ); ?></textarea>
						<?php if (isset($malinky_error_messages['message_error'][0])) echo '<p class="box error">' . $malinky_error_messages['message_error'][0] . '</p>'; ?>
						<?php wp_nonce_field( 'malinky_process_contact_form', 'malinky_process_contact_form_nonce' ); ?>
						
						<input type="submit" value="Submit" name="submit_contact" class="alignright">

					</form>

					<?php if ( isset( $_GET['contact'] ) && $_GET['contact'] == 'success' ) { ?>
						
						<?php
						$contact_form_page = get_option( '_000002_contact_form_settings' );
						?>

						<div class="box success">
							<?php echo empty( $contact_form_page['contacted_message'] ) ? 'Thanks for contacting us, we will be in touch as soon as possible.' : $contact_form_page['contacted_message']; ?>
						</div>

					<?php } ?>

				</div>
			</div>

		</div><!-- .wrap -->
	</section><!-- .wrap-full -->

	<?php return ob_get_clean(); ?>
	
<?php
}

add_shortcode( 'malinky-contact-form', 'malinky_contact_form_shortcode' );


/**
 * Called from header.php to check if on the selected contact form page for form processing.
 */
function malinky_contact_form_header()
{

	$contact_form_page = get_option( '_000002_contact_form_settings' );
	
	/*
	 * Check it exists (isn't false) then if on the correct page.
	 */
	if ( ! empty( $contact_form_page['contact_form_page'] ) && is_page( $contact_form_page['contact_form_page'] ) ) {

		$error_messages 	= array();
		$name 				= isset( $_POST['malinky_name'] ) ? $_POST['malinky_name'] : '';
		$company			= isset( $_POST['malinky_company'] ) ? $_POST['malinky_company'] : '';
		$email 				= isset( $_POST['malinky_email'] ) ? $_POST['malinky_email'] : '';
		$phone 				= isset( $_POST['malinky_phone'] ) ? $_POST['malinky_phone'] : '';	
		$message 			= isset( $_POST['malinky_message'] ) ? $_POST['malinky_message'] : '';

		if ( ! empty( $_POST['submit_contact'] ) ) {	
			$errors = malinky_contact_form_process( $name, $company, $email, $phone, $message );
			if ( is_wp_error( $errors ) ) {
				global $malinky_error_messages;
				$malinky_error_messages = $errors->errors;
			} else {
				$success = $errors;
			}
		}

	}

}


/**
 * Process the contact form and error checking.
 * 
 * @param string $name 
 * @param string $company
 * @param string $phone
 * @param string $email  
 * @param string $message     
 * @return void
 */
function malinky_contact_form_process( $name, $company, $email, $phone, $message )
{

	//check form submit and nonce
	if ( ( empty($_POST['submit_contact'] ) ) || ( ! isset( $_POST['malinky_process_contact_form_nonce'] ) ) || ( ! wp_verify_nonce( $_POST['malinky_process_contact_form_nonce'], 'malinky_process_contact_form' ) ) )
		wp_die( __( 'There was a fatal error with your form submission' ) );

	$errors = new WP_Error();
	$contact_error_messages = malinky_contact_form_error_messages();

	$sanitized_name 		= sanitize_text_field( $name );
	$sanitized_company 		= sanitize_text_field( $company );
	$sanitized_email 		= sanitize_email( $email );	
	$sanitized_phone 		= sanitize_text_field( $phone );	
	$sanitized_message 		= sanitize_text_field( $message );			

	//check the first_name
	if ( $sanitized_name == '' ) {
		$errors->add( 'name_error', __( $contact_error_messages['name_error'] ) );
	}

	//check the email
	if ( $sanitized_email == '' ) {
		$errors->add( 'email_error', __( $contact_error_messages['email_error'] ) );
	} elseif ( ! is_email( $sanitized_email ) ) {
		$errors->add('email_error', __( $contact_error_messages['email_error'] ) );
	}

	//check the phone
	if ( $sanitized_phone != '' ) {
		if ( ! preg_match( '/[0-9 ]+$/', $sanitized_phone ) ) {		
			$errors->add( 'phone_error', __( $contact_error_messages['phone_error_2'] ) );
		}
	}		

	//check the message
	if ( $sanitized_message == '' ) {
		$errors->add( 'message_error', __( $contact_error_messages['message_error'] ) );
	}

	//if validation errors
	if ( $errors->get_error_code() )
		return $errors;

	//send registration email
	malinky_contact_form_email( $sanitized_name, $sanitized_company, $sanitized_email, $sanitized_phone, $sanitized_message );

	global $post;
	wp_safe_redirect( $post->post_name . '?contact=success#contact-success', 303 );

}


/**
 * Contact form errors.
 */
function malinky_contact_form_error_messages()
{

	return array(
		'name_error'		=> 'Please enter your name.',
		'email_error'		=> 'Please enter a valid email address.',		
		'phone_error'		=> 'Please enter your phone number.',
		'phone_error_2'		=> 'Please enter a valid phone number.',
		'message_error'		=> 'Please enter a message.',
	);

}


/**
 * Send the contact form email.
 * 
 * @param string $sanitized_name  
 * @param string $sanitized_company  
 * @param string $sanitized_email  
 * @param string $sanitized_phone
 * @param string $sanitized_message      
 * @return void   
 */
function malinky_contact_form_email( $sanitized_name, $sanitized_company, $sanitized_email, $sanitized_phone, $sanitized_message )
{

	include_once( ABSPATH . WPINC . '/class-phpmailer.php' );

	$email_settings = get_option( '_000002_contact_form_settings' );

	if ( ! $email_settings ) return;
	
	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['email_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['email_port'];
	$mail->Username   	= $email_settings['email_address'];
	$mail->Password   	= $email_settings['email_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress( get_option( 'admin_email' ), get_bloginfo( 'name' ) );

	$mail->SetFrom( get_option( 'admin_email' ), get_bloginfo( 'name' ) );

	$mail->IsHTML(true);

	$mail->Subject 	= get_bloginfo( 'name' ) . ' Contact Form Message';
	$mail->Body    	= '<p>Name: ' . $sanitized_name . '</p>';
	$mail->Body    .= '<p>Company: ' . $sanitized_email . '</p>';
	$mail->Body    .= '<p>Email: ' . $sanitized_email . '</p>';
	$mail->Body    .= '<p>Phone: ' . $sanitized_phone . '</p>';
	$mail->Body    .= '<p>Message: ' . $sanitized_message . '</p>';			

	if( ! $mail->Send() ) {
		error_log( $mail->ErrorInfo, 0 );
    }

}