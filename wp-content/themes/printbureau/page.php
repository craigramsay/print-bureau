<?php
/**
 * The template for displaying all pages. Full width for print-bureau.co.uk
 */

get_header(); ?>

<?php if ( have_rows( 'carousel_images' ) ) {

	$carousel_count = 0;

    while ( have_rows( 'carousel_images' ) ) : the_row();

        $carousel_images = get_sub_field( 'image' );
        $carousel[ $carousel_count ]['large'] 	= $carousel_images['sizes']['malinky_carousel_large'];
        $carousel[ $carousel_count ]['medium'] 	= $carousel_images['sizes']['malinky_carousel_medium'];
        $carousel[ $carousel_count ]['small'] 	= $carousel_images['sizes']['malinky_carousel_small'];

	$carousel_count++;

    endwhile;

    if ( malinky_is_phone() ) {
    	$carousel_size = 'small';
	} elseif ( malinky_is_tablet() ) {
		$carousel_size = 'medium';
	} else {
		$carousel_size = 'large';
	} ?>

	<?php if ( count( get_field( 'carousel_images' ) ) > 1 ) { ?>
	<div class="wrap-full wrap-mobile">
		<div id="owl-carousel-id">
			<?php $carousel_count = 0;
		    while ( have_rows( 'carousel_images' ) ) : the_row(); ?>
			<div>
				<img src="<?php echo esc_url( $carousel[ $carousel_count ][ $carousel_size ] ); ?>" class="malinky-fade-in" alt="<?php echo esc_attr( bloginfo( 'name' ) ); ?> Carousel" />
			</div>
			<?php $carousel_count++;
			endwhile; ?>
		</div>
	</div>
	<?php } else { ?>
		<div class="wrap-full wrap-mobile">
			<?php $carousel_count = 0;
			while ( have_rows( 'carousel_images' ) ) : the_row(); ?>
				<div>
					<img src="<?php echo esc_url( $carousel[ $carousel_count ][ $carousel_size ] ); ?>" alt="<?php echo esc_attr( bloginfo( 'name' ) ); ?> Carousel" />
				</div>
			<?php endwhile; ?>
		</div>
	<?php }	

} ?>

<main role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

	<?php endwhile; //end loop. ?>

</main><!-- #main -->

<?php get_footer(); ?>