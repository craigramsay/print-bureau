<?php
/**
 * The template part for displaying a page in page.php.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php the_content();

	/* ------------------------------------------------------------------------ *
	 * Flexible Content
	 * ------------------------------------------------------------------------ */

	if ( have_rows( 'block' ) ):

	    while ( have_rows( 'block' ) ) : the_row();

	        if ( get_row_layout() == 'text_block' ) { ?>

		        <div class="wrap wrap--padding">
					<div class="col">
						<div class="col-item col-item-full">
							<?php the_sub_field( 'content' ); ?>
						</div>
					</div>
				</div>
	        
	        <?php } elseif ( get_row_layout() == 'text_block_with_background' ) { ?>

	        	<?php
	        	$bkg_class = get_sub_field( 'background' );
	        	$bkg_class = $bkg_class['title'];
	        	$bkg_position = 'bkg--position-' . get_sub_field( 'background_position' );
	        	?>

		        <div class="full-wrap <?php echo esc_attr( $bkg_class . ' ' . $bkg_position ); ?>">
			        <div class="wrap wrap--padding">
						<div class="col">
							<div class="col-item col-item-full">
								<?php the_sub_field( 'content' ); ?>
							</div>
						</div>
					</div>
				</div>

	        <?php } elseif ( get_row_layout() == 'service_block' ) { ?>

				<?php
				/*
				 * Get services position outside of nested have_rows( 'service' ).
				 */
				$services_position = '';
				$services_position = get_sub_field( 'services_position' );
				/*
				 * Get services count here. Although a repeater field it's a sub_field of service_block.have_rows( 'service' ).
				 */
				$services_count = '';
				$services_count = count( get_sub_field( 'service' ) );
				?>

		        <div class="wrap wrap--padding">

					<div class="col">
						<div class="col-item col-item-full col-item--margin-bottom-40">
							<?php the_sub_field( 'content' ); ?>
						</div>
					</div>

					<?php if ( have_rows( 'service' ) ): ?>

						<div class="col <?php echo $services_position == 'center' ? 'col--align-center' : ''; ?>">

						 	<?php
						 	/*
						 	 * ($services_count & 1) is a bitwise check. An odd number $services_count returns true or an even number $services_count false.
						 	 */
						 	while ( have_rows( 'service' ) ) : the_row(); ?><div class="col-item <?php echo ($services_count & 1 || $services_count % 3 == 0) ? 'col-item-third col-item-half--medium col-item-full--small col-item--margin-bottom-40' : 'col-item-half col-item-full--small col-item--margin-bottom-40'; ?>">
						        	
						        	<div class="service_image">
						        		<img src="<?php echo esc_url( wp_get_attachment_url( get_sub_field( 'image' ) ) ); ?>" <?php echo get_sub_field( 'image_shape' ) == 'circle' ? ' class="service_image__circle"' : ''; ?> alt="Print Bureau Services" />
						        	</div>

						        	<?php the_sub_field( 'content' ); ?>

							</div><?php endwhile; ?>

						</div><!-- .col -->

					<?php endif; ?>

				</div><!-- .wrap -->

	       	<?php } elseif ( get_row_layout() == 'google_map' ) { ?>

		        <div class="wrap-full">
					<?php echo  do_shortcode( '[' . get_sub_field( 'shortcode' ) . ']' ); ?>
				</div>

	        <?php }

	    endwhile;

	else :


	endif;

	/* ------------------------------------------------------------------------ *
	 * End Flexible Content
	 * ------------------------------------------------------------------------ */

	?>

</article><!-- #post-## -->