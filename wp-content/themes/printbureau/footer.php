<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Malinky Media
 */
?>

<?php $contact_settings = get_option( '_000001_contact_information' ); ?>

<footer class="main-footer wrap-full" role="contentinfo">

	<div class="wrap">
		<div class="col">
			<div class="col-item col-item-quarter col-item-full--medium col-item--align-center--medium col-item--margin-bottom-40--medium col-item-full--small col-item--align-center--small col-item--margin-bottom-40--small">
				<h3>Contact Us</h3>
				<p><?php echo esc_html( $contact_settings['email_address'] ); ?><br />
				<?php echo esc_html( $contact_settings['phone_number'] ); ?><br />
				<?php echo $contact_settings['address']; ?>
				<h3>We're Open</h3>
				<p><?php echo esc_html( $contact_settings['opening_days'] ); ?><br/ >
			    <?php echo esc_html( $contact_settings['opening_hours'] ); ?></p>
			</div><!--
			--><div class="col-item col-item-3-8 col-item-full--medium col-item--align-center--medium col-item--margin-bottom-40--medium col-item-full--small col-item--align-center--small col-item--margin-bottom-40--small">
				<h3>Social Media</h3>
				<a href="<?php echo esc_html( $contact_settings['facebook_account'] ); ?>" class="image-font" target="_blank"><span class="image-font__sizing image-font__sizing--social-media image-font__fontawesome fa-facebook"></span></a>
				<a href="<?php echo esc_html( $contact_settings['twitter_account'] ); ?>" class="image-font" target="_blank"><span class="image-font__sizing image-font__sizing--social-media image-font__fontawesome fa-twitter"></span></a>
				<a href="<?php echo esc_html( $contact_settings['google_plus_account'] ); ?>" class="image-font" target="_blank"><span class="image-font__sizing image-font__sizing--social-media image-font__fontawesome fa-google-plus"></span></a>
				<a href="<?php echo esc_html( $contact_settings['linkedin_account'] ); ?>" class="image-font" target="_blank"><span class="image-font__sizing image-font__sizing--social-media image-font__fontawesome fa-linkedin"></span></a>
			</div><!--
			--><div class="col-item col-item-3-8 col-item-full--medium col-item-full--small">
				<!-- Begin MailChimp Signup Form -->
				<div id="mc_embed_signup">
					<form action="//print-bureau.us9.list-manage.com/subscribe/post?u=f2e1d7416506d1c0c80dc22e0&amp;id=e70b025f51" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
					    <div id="mc_embed_signup_scroll">
							<h3 class="col-item--align-center--medium col-item--align-center--small">Newsletter Signup</h3>
							<div class="mc-field-group">
								<label for="mce-EMAIL">Email Address </label>
								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" required>
							</div>
							<div class="mc-field-group">
								<label for="mce-FNAME">First Name </label>
								<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
							</div>
							<div class="mc-field-group">
								<label for="mce-LNAME">Last Name </label>
								<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
							</div>		
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;"><input type="text" name="b_f2e1d7416506d1c0c80dc22e0_e70b025f51" tabindex="-1" value=""></div>
						    <div class="clear"><input type="submit" value="Sign me up!" name="subscribe" id="mc-embedded-subscribe" class="button alignright"></div>
					    </div>
					</form>
				</div>
				<!--End mc_embed_signup-->
			</div>
		</div>
	</div><!-- .wrap -->

</footer><!-- .main-footer -->

<footer class="secondary-footer wrap-full" role="contentinfo">

	<div class="wrap">
		<div class="col">
			<div class="col-item col-item-half col-item-full--medium col-item--align-center--medium col-item--margin-bottom-20--medium col-item-full--small col-item--align-center--small col-item--margin-bottom-20--small main-footer__navigation">
				<nav class="footer-navigation" role="navigation">
					<?php
			        $args = array(
			            'theme_location'    => 'footer_navigation',
			            'container'   		=> 'false'
			        );
			        ?>
					<?php wp_nav_menu( $args ); ?>
				</nav><!-- .main_navigation -->
			</div><!--
			--><div class="col-item col-item-half col--align-right col-item-full--medium col-item--align-center--medium col-item-full--small col-item--align-center--small main-footer__copyright">
				<?php echo bloginfo( 'name' ); ?> &copy; <?php echo date('Y'); ?>
			</div>
		</div>
	</div><!-- .wrap -->

</footer><!-- .main-footer -->

<a class="back-top image-font__fontawesome fa-angle-up"></a>

<?php if ( WP_ENV == 'prod' ) { ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create',"<?php echo get_option( '_000003_google_analytics' ); ?>",'auto');ga('send','pageview');
</script>

<?php } ?>
		
<?php wp_footer(); ?>

<?php if ( WP_ENV == 'local' ) { ?>

<!-- Live Reload -->
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

<?php } ?>

</body>
</html>
